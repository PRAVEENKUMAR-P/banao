FROM python:3.9

WORKDIR /app

COPY hello_world_server.py .

EXPOSE 8000

CMD ["python", "hello_world_server.py"]
